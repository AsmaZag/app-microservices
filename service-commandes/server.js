const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const commandes = require('./routes/api/commandes');

const app = express();

app.use(cors());

app.use(express.json());

app.use('/api/commandes' , commandes);

const port = process.env.PORT || 3001;

// Démarrage du serveur
app.listen(port, () => console.log(`Serveur démarré sur le port ${port}`) );
